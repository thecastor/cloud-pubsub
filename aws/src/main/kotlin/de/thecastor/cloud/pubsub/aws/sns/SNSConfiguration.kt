package de.thecastor.cloud.pubsub.aws.sns

import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.regions.Region
import de.thecastor.cloud.pubsub.base.Configuration

class SNSConfiguration(val credentialsProvider: AWSCredentialsProvider,
                       val region: Region,
                       val topicName: String
) : Configuration