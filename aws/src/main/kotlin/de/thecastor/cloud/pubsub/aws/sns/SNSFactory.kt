package de.thecastor.cloud.pubsub.aws.sns

import com.amazonaws.services.sns.AmazonSNSClient
import com.amazonaws.services.sns.model.CreateTopicRequest
import de.thecastor.cloud.pubsub.base.PubSubFactory
import de.thecastor.cloud.pubsub.base.Topic
import java.net.URI


class SNSFactory : PubSubFactory<Unit, String, SNSConfiguration> {

    override fun getTopic(config: SNSConfiguration) = createTopic(config)

    override fun createTopic(config: SNSConfiguration): Topic<Unit, String> {
        val client = AmazonSNSClient.builder().apply {
            region = config.region.name
            credentials = config.credentialsProvider
        }.build()

        val createTopicRequest = CreateTopicRequest(config.topicName)

        // if the topic existed before, the existing topic is returned
        val createTopicResult = client.createTopic(createTopicRequest)

        return SNSTopic(config.topicName, URI.create(createTopicResult.topicArn), client)
    }

}