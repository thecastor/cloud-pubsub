package de.thecastor.cloud.pubsub.aws.sns

import de.thecastor.cloud.pubsub.base.PublishRequest

class SNSPublishRequest(override val data: String) : PublishRequest<String>