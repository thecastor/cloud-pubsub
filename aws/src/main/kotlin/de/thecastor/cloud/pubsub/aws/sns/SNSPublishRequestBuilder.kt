package de.thecastor.cloud.pubsub.aws.sns

import de.thecastor.cloud.pubsub.base.AbstractPublishRequestBuilder
import de.thecastor.cloud.pubsub.base.PublishRequestBuilder
import java.nio.charset.Charset

class SNSPublishRequestBuilder : AbstractPublishRequestBuilder<String>() {

    override fun setData(data: String, charset: Charset): SNSPublishRequestBuilder {
        this.data = data

        return this
    }

    override fun setData(data: ByteArray) = setData(data.toString(PublishRequestBuilder.DEFAULT_CHARSET), PublishRequestBuilder.DEFAULT_CHARSET)

    override fun build(): SNSPublishRequest {
        val data = data ?: throw IllegalStateException("data is not set")

        return SNSPublishRequest(data)
    }

}