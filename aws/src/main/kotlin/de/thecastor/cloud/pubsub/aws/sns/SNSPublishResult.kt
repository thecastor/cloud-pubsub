package de.thecastor.cloud.pubsub.aws.sns

import de.thecastor.cloud.pubsub.base.PublishCallback
import de.thecastor.cloud.pubsub.base.PublishResult

internal class SNSPublishResult(private val callback: PublishCallback) : PublishResult, PublishCallback by callback {

    var failure: Boolean = false
        internal set

    override fun onFailure(cause: Throwable) {
        failure = true

        callback.onFailure(cause)
    }

    override fun cancel() {}

    override fun isCancelled() = false

    override fun isSuccess() = !failure

    override fun isFailure() = failure

    override fun isComplete() = true

}