package de.thecastor.cloud.pubsub.aws.sns

import com.amazonaws.services.sns.AmazonSNS
import de.thecastor.cloud.pubsub.base.PublishCallback
import de.thecastor.cloud.pubsub.base.PublishRequest
import de.thecastor.cloud.pubsub.base.PublishResult
import de.thecastor.cloud.pubsub.base.Publisher

internal class SNSPublisher(private val client: AmazonSNS, private val topicArn: String) : Publisher<String> {

    override fun requestBuilder() = SNSPublishRequestBuilder()

    override fun sendAsync(publishRequest: PublishRequest<String>, callback: PublishCallback): PublishResult {
        val nativeRequest = com.amazonaws.services.sns.model.PublishRequest(topicArn, publishRequest.data)
        val result = SNSPublishResult(callback)

        try {
            val publishResult = client.publish(nativeRequest)

            result.onSuccess(publishResult.messageId)
        } catch (e: Exception) {
            result.onFailure(e)
        }

        return result
    }

}