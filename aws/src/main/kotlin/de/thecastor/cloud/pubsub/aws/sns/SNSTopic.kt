package de.thecastor.cloud.pubsub.aws.sns

import com.amazonaws.services.sns.AmazonSNS
import de.thecastor.cloud.pubsub.aws.sns.sub.SNSSubscriber
import de.thecastor.cloud.pubsub.base.Publisher
import de.thecastor.cloud.pubsub.base.Subscriber
import de.thecastor.cloud.pubsub.base.Topic
import java.net.URI

class SNSTopic(override val name: String,
               topicArn: URI,
               client: AmazonSNS
) : Topic<Unit, String>,
    Publisher<String> by SNSPublisher(client = client, topicArn = topicArn.toString()),
    Subscriber<Unit, String> by SNSSubscriber() {

    override val uri: URI? = topicArn

}