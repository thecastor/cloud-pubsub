package de.thecastor.cloud.pubsub.aws.sns.sub

import de.thecastor.cloud.pubsub.base.EventProcessor
import de.thecastor.cloud.pubsub.base.Subscriber
import de.thecastor.cloud.pubsub.base.Subscription

class SNSSubscriber : Subscriber<Unit, String> {

    override fun subscribe(processor: EventProcessor<Unit, String>): Subscription<String> {
        throw UnsupportedOperationException("not implemented")
    }

}