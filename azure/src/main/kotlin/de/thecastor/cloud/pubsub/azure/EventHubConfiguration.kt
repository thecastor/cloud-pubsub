package de.thecastor.cloud.pubsub.azure

import com.microsoft.azure.eventhubs.ConnectionStringBuilder
import de.thecastor.cloud.pubsub.base.Configuration
import java.net.URI
import java.time.Duration
import java.util.concurrent.ThreadPoolExecutor

class EventHubConfiguration(val executor: ThreadPoolExecutor,
                            val hostNamePrefix: String,
                            val storageConnectionString: String,
                            val storageContainerName: String,
                            val namespaceName: String,
                            val eventHubName: String,
                            val consumerGroupName: String,
                            val sharedAccessSignatureKeyName: String,
                            val sharedAccessSignatureKey: String,
                            val sharedAccessSignature: String? = null,
                            val endpoint: URI? = null,
                            val operationTimeout: Duration? = null) : Configuration {

    val eventHubConnectionString: String =
            ConnectionStringBuilder().also { builder ->
                builder.setNamespaceName(namespaceName)
                builder.eventHubName = eventHubName
                builder.sasKeyName = sharedAccessSignatureKeyName
                builder.sasKey = sharedAccessSignatureKey

                sharedAccessSignature?.apply { builder.sharedAccessSignature = this }
                endpoint?.apply { builder.endpoint = this }
                operationTimeout?.apply { builder.operationTimeout = this }

            }.toString()

    val name = eventHubConnectionString

}