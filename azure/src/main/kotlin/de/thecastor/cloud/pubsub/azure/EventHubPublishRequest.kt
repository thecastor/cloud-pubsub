package de.thecastor.cloud.pubsub.azure

import de.thecastor.cloud.pubsub.base.PublishRequest

class EventHubPublishRequest(override val data: ByteArray) : PublishRequest<ByteArray>