package de.thecastor.cloud.pubsub.azure

import de.thecastor.cloud.pubsub.base.AbstractPublishRequestBuilder

class EventHubPublishRequestBuilder : AbstractPublishRequestBuilder<ByteArray>() {

    override fun setData(data: ByteArray): EventHubPublishRequestBuilder {
        this.data = data

        return this
    }

    override fun build(): EventHubPublishRequest {
        val data = data ?: throw IllegalStateException("data is not set")

        return EventHubPublishRequest(data)
    }

}