package de.thecastor.cloud.pubsub.azure

import com.microsoft.azure.eventhubs.EventData
import com.microsoft.azure.eventhubs.EventHubClient
import de.thecastor.cloud.pubsub.base.PublishCallback
import de.thecastor.cloud.pubsub.base.PublishRequest
import de.thecastor.cloud.pubsub.base.PublishResult
import de.thecastor.cloud.pubsub.base.Publisher

internal class EventHubPublisher(private val client: EventHubClient) : Publisher<ByteArray> {

    override fun requestBuilder() = EventHubPublishRequestBuilder()

    override fun sendAsync(publishRequest: PublishRequest<ByteArray>, callback: PublishCallback): PublishResult {
        val future = client.send(EventData.create(publishRequest.data))
        val result = PublishResultImpl(future, callback)

        future.whenCompleteAsync({ _, cause ->
            if (cause != null) {
                result.onFailure(cause)
            } else {
                result.onSuccess()
            }
        })

        return result
    }

}