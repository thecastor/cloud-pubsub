package de.thecastor.cloud.pubsub.azure

import de.thecastor.cloud.pubsub.base.PubSubFactory
import de.thecastor.cloud.pubsub.base.Topic

class EventHubsFactory : PubSubFactory<Unit, ByteArray, EventHubConfiguration> {

    private val pubSubs = hashMapOf<String, Topic<Unit, ByteArray>>()

    override fun getTopic(config: EventHubConfiguration): Topic<Unit, ByteArray> {
        val name = config.eventHubConnectionString

        return pubSubs.getOrPut(name) {
            EventHubsTopic(config)
        }
    }

    //
    // REST API:
    //   https://docs.microsoft.com/en-us/rest/api/eventhub/eventhubs/createorupdate
    //
    override fun createTopic(config: EventHubConfiguration): Topic<Unit, ByteArray> {
        throw UnsupportedOperationException("not implemented")
    }

}