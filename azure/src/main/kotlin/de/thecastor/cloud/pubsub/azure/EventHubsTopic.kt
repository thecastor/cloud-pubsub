package de.thecastor.cloud.pubsub.azure

import com.microsoft.azure.eventhubs.EventHubClient
import de.thecastor.cloud.pubsub.azure.sub.EventHubSubscriber
import de.thecastor.cloud.pubsub.base.Publisher
import de.thecastor.cloud.pubsub.base.Subscriber
import de.thecastor.cloud.pubsub.base.Topic
import java.net.URI

class EventHubsTopic(config: EventHubConfiguration) : Topic<Unit, ByteArray>,
                                                      Publisher<ByteArray> by EventHubPublisher(EventHubClient.createSync(config.name, config.executor)),
                                                      Subscriber<Unit, ByteArray> by EventHubSubscriber(config) {

    override val name: String = config.name
    override val uri: URI? = null

}