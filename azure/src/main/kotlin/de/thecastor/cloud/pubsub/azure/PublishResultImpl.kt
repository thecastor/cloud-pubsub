package de.thecastor.cloud.pubsub.azure

import de.thecastor.cloud.pubsub.base.PublishCallback
import de.thecastor.cloud.pubsub.base.PublishResult
import java.util.concurrent.CompletableFuture

internal class PublishResultImpl(private val future: CompletableFuture<Void>,
                                 callback: PublishCallback
) : PublishResult, PublishCallback by callback {

    override fun cancel() {
        future.cancel(true)
    }

    override fun isCancelled() = future.isCancelled

    override fun isSuccess() = isComplete() && !isFailure() && !isCancelled()

    override fun isFailure() = future.isCompletedExceptionally && !isCancelled()

    override fun isComplete() = future.isDone

}