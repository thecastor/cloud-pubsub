package de.thecastor.cloud.pubsub.azure.sub

import com.microsoft.azure.eventprocessorhost.ExceptionReceivedEventArgs
import de.thecastor.cloud.pubsub.base.EventProcessor
import de.thecastor.cloud.pubsub.base.SubscriptionHostException
import java.util.function.Consumer

class ErrorNotificationHandler(private val processor: EventProcessor<*, *>) : Consumer<ExceptionReceivedEventArgs> {
    override fun accept(t: ExceptionReceivedEventArgs) {
//        println("SAMPLE: Host " + t.hostname + " received general error notification during " + t.action + ": " + t.exception.toString())
        processor.onError(SubscriptionHostException("Host ${t.hostname} received general error notification during ${t.action}: ${t.exception.javaClass.name}", t.exception))
    }
}