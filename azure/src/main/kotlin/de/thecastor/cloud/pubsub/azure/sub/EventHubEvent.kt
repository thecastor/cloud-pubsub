package de.thecastor.cloud.pubsub.azure.sub

import de.thecastor.cloud.pubsub.base.Event

class EventHubEvent(override val data: ByteArray) : Event<Unit, ByteArray> {

    override val id = Unit

}