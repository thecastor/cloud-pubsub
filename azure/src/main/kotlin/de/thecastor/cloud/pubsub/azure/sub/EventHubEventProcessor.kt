package de.thecastor.cloud.pubsub.azure.sub

import com.microsoft.azure.eventhubs.EventData
import com.microsoft.azure.eventprocessorhost.CloseReason
import com.microsoft.azure.eventprocessorhost.IEventProcessor
import com.microsoft.azure.eventprocessorhost.PartitionContext
import de.thecastor.cloud.pubsub.base.EventProcessor
import de.thecastor.cloud.pubsub.base.SubscriptionException

abstract class EventHubEventProcessor : EventProcessor<Unit, ByteArray> {

    internal class NativeEventProcessor(private val processor: EventProcessor<Unit, ByteArray>) : IEventProcessor {

        private var checkpointBatchingCount = 0

        // OnOpen is called when a new event processor instance is created by the host. In a real implementation, this
        // is the place to do initialization so that events can be processed when they arrive, such as opening a database
        // connection.
        @Throws(Exception::class)
        override fun onOpen(context: PartitionContext) {
//            System.out.println("SAMPLE: Partition " + context.getPartitionId() + " is opening")
            processor.onOpen(context)
        }

        // OnClose is called when an event processor instance is being shut down. The reason argument indicates whether the shut down
        // is because another host has stolen the lease for this partition or due to error or host shutdown. In a real implementation,
        // this is the place to do cleanup for resources that were opened in onOpen.
        @Throws(Exception::class)
        override fun onClose(context: PartitionContext, reason: CloseReason) {
//            System.out.println("SAMPLE: Partition " + context.getPartitionId() + " is closing for reason " + reason.toString())
            processor.onClose(reason)
        }

        // onError is called when an error occurs in EventProcessorHost code that is tied to this partition, such as a receiver failure.
        // It is NOT called for exceptions thrown out of onOpen/onClose/onEvents. EventProcessorHost is responsible for recovering from
        // the error, if possible, or shutting the event processor down if not, in which case there will be a call to onClose. The
        // notification provided to onError is primarily informational.
        override fun onError(context: PartitionContext, error: Throwable) {
//            System.out.println("SAMPLE: Partition " + context.getPartitionId() + " onError: " + error.toString())
            processor.onError(SubscriptionException(error))
        }

        // onEvents is called when events are received on this partition of the Event Hub. The maximum number of events in a batch
        // can be controlled via EventProcessorOptions. Also, if the "invoke processor after receive timeout" option is set to true,
        // this method will be called with null when a receive timeout occurs.
        @Throws(Exception::class)
        override fun onEvents(context: PartitionContext, events: Iterable<EventData>) {
//            System.out.println("SAMPLE: Partition " + context.getPartitionId() + " got event batch")

            var eventCount = 0

            for (data in events) {
                // It is important to have a try-catch around the processing of each event. Throwing out of onEvents deprives
                // you of the chance to process any remaining events in the batch.
                try {
//                    System.out.println("SAMPLE (" + context.getPartitionId() + "," + data.systemProperties.offset + "," +
//                            data.systemProperties.sequenceNumber + "): " + String(data.bytes, Charsets.UTF_8))
                    processor.onEvent(EventHubEvent(data.bytes))
                    eventCount++

                    // Checkpointing persists the current position in the event stream for this partition and means that the next
                    // time any host opens an event processor on this event hub+consumer group+partition combination, it will start
                    // receiving at the event after this one. Checkpointing is usually not a fast operation, so there is a tradeoff
                    // between checkpointing frequently (to minimize the number of events that will be reprocessed after a crash, or
                    // if the partition lease is stolen) and checkpointing infrequently (to reduce the impact on event processing
                    // performance). Checkpointing every five events is an arbitrary choice for this sample.
                    this.checkpointBatchingCount++

                    if (checkpointBatchingCount % 5 == 0) {
//                        System.out.println("SAMPLE: Partition " + context.getPartitionId() + " checkpointing at " +
//                                data.systemProperties.offset + "," + data.systemProperties.sequenceNumber)
                        // Checkpoints are created asynchronously. It is important to wait for the result of checkpointing
                        // before exiting onEvents or before creating the next checkpoint, to detect errors and to ensure proper ordering.
                        context.checkpoint(data).get()
                    }
                } catch (e: Exception) {
//                    println("Processing failed for an event: " + e.toString())
                    processor.onError(SubscriptionException("Error during event processing ", e))
                }
            }

//            System.out.println("SAMPLE: Partition " + context.getPartitionId() + " batch size was " + eventCount + " for host " + context.getOwner())
        }
    }
}
