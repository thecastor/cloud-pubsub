package de.thecastor.cloud.pubsub.azure.sub

import de.thecastor.cloud.pubsub.azure.EventHubConfiguration
import de.thecastor.cloud.pubsub.base.EventProcessor
import de.thecastor.cloud.pubsub.base.Subscriber

class EventHubSubscriber(private val config: EventHubConfiguration) : Subscriber<Unit, ByteArray> {

    override fun subscribe(processor: EventProcessor<Unit, ByteArray>) = EventHubSubscription(config, processor)

}