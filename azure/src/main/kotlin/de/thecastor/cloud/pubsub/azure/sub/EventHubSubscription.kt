package de.thecastor.cloud.pubsub.azure.sub

import com.microsoft.azure.eventprocessorhost.EventProcessorHost
import com.microsoft.azure.eventprocessorhost.EventProcessorOptions
import de.thecastor.cloud.pubsub.azure.EventHubConfiguration
import de.thecastor.cloud.pubsub.base.EventProcessor
import de.thecastor.cloud.pubsub.base.Subscription
import de.thecastor.cloud.pubsub.base.SubscriptionHostException

class EventHubSubscription(config: EventHubConfiguration,
                           private val processor: EventProcessor<Unit, ByteArray>
) : Subscription<ByteArray> {

    private val eventProcessorHost: EventProcessorHost =
            EventProcessorHost(
                    EventProcessorHost.createHostName(config.hostNamePrefix),
                    config.eventHubName,
                    config.consumerGroupName,
                    config.eventHubConnectionString,
                    config.storageConnectionString,
                    config.storageContainerName).also {
                val options = EventProcessorOptions()
                options.setExceptionNotification(ErrorNotificationHandler(processor))

                it.registerEventProcessorFactory(EventProcessorFactory(processor), options)
                        .whenCompleteAsync({ _, t ->
                            if (t != null) {
                                processor.onError(SubscriptionHostException("Registration of EventProcessor failed", t))
                            }
                        })
            }

    private var _cancelled = false
    override val cancelled: Boolean
        get() = _cancelled

    override fun cancel() {
        if (cancelled) return

        eventProcessorHost.unregisterEventProcessor()
                .whenComplete { _, t ->
                    if (t != null) {
                        processor.onError(SubscriptionHostException("Failure while unregistering", t))
                    } else {
                        _cancelled = true
                    }
                }
    }

}