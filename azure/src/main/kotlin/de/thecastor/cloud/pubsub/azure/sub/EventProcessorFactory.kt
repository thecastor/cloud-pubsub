package de.thecastor.cloud.pubsub.azure.sub

import com.microsoft.azure.eventprocessorhost.IEventProcessorFactory
import com.microsoft.azure.eventprocessorhost.PartitionContext
import de.thecastor.cloud.pubsub.base.EventProcessor

internal class EventProcessorFactory(private val eventProcessor: EventProcessor<Unit, ByteArray>) : IEventProcessorFactory<EventHubEventProcessor.NativeEventProcessor> {

    override fun createEventProcessor(context: PartitionContext?): EventHubEventProcessor.NativeEventProcessor {
        return EventHubEventProcessor.NativeEventProcessor(eventProcessor)
    }

}