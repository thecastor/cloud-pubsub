package de.thecastor.cloud.pubsub.base

import java.nio.charset.Charset

abstract class AbstractPublishRequestBuilder<T> : PublishRequestBuilder<T> {

    protected var data: T? = null

    override fun setData(data: String, charset: Charset) = setData(data.toByteArray(charset))

}