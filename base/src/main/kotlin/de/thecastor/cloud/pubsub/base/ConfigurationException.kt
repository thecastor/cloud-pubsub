package de.thecastor.cloud.pubsub.base

class ConfigurationException(message: String, cause: Throwable?) : RuntimeException(message, cause) {

    constructor(message: String) : this(message, null)

}