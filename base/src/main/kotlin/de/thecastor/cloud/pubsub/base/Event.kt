package de.thecastor.cloud.pubsub.base

interface Event<IdType, T> {

    val id: IdType?
    val data: T

}