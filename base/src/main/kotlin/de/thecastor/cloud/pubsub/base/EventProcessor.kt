package de.thecastor.cloud.pubsub.base

interface EventProcessor<IdType, T> {

    fun onOpen(vararg args: Any)

    fun onClose(vararg args: Any)

    fun onError(t: SubscriptionException)

    fun onEvent(event: Event<IdType, T>)

}