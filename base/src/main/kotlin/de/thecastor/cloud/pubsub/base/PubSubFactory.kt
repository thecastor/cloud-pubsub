package de.thecastor.cloud.pubsub.base

interface PubSubFactory<IdType, T, C : Configuration> {

    fun getTopic(config: C): Topic<IdType, T>

    fun createTopic(config: C): Topic<IdType, T>

//    fun deleteTopic(topic: Topic, config: C? = null)

}