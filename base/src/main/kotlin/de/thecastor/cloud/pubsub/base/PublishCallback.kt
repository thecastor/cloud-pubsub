package de.thecastor.cloud.pubsub.base

interface PublishCallback {

    fun onSuccess()

    fun onSuccess(result: Any?)

    fun onFailure(cause: Throwable)

    fun onCancel()

}