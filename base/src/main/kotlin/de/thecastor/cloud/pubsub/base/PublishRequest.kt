package de.thecastor.cloud.pubsub.base

interface PublishRequest<T> {

    val data: T

}