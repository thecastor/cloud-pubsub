package de.thecastor.cloud.pubsub.base

import java.nio.charset.Charset

interface PublishRequestBuilder<T> {

    fun setData(data: String, charset: Charset): PublishRequestBuilder<T>

    fun setData(data: String) = setData(data, DEFAULT_CHARSET)

    fun setData(data: ByteArray): PublishRequestBuilder<T>

    fun build(): PublishRequest<T>

    companion object {
        val DEFAULT_CHARSET = Charsets.UTF_8
    }

}