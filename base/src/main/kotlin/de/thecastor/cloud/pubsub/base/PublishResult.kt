package de.thecastor.cloud.pubsub.base

interface PublishResult : PublishCallback {

    fun cancel()

    fun isCancelled(): Boolean

    fun isSuccess(): Boolean

    fun isFailure(): Boolean

    fun isComplete(): Boolean

}