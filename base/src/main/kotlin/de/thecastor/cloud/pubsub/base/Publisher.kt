package de.thecastor.cloud.pubsub.base

interface Publisher<T> {

    fun requestBuilder(): PublishRequestBuilder<T>

    fun sendAsync(publishRequest: PublishRequest<T>, callback: PublishCallback): PublishResult

}