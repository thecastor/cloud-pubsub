package de.thecastor.cloud.pubsub.base

interface Subscriber<IdType, T> {

    fun subscribe(processor: EventProcessor<IdType, T>): Subscription<T>

}