package de.thecastor.cloud.pubsub.base

interface Subscription<T> {

    val cancelled: Boolean

    fun cancel()

}