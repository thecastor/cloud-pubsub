package de.thecastor.cloud.pubsub.base

class SubscriptionHostException(message: String, cause: Throwable) : SubscriptionException(message, cause)