package de.thecastor.cloud.pubsub.base

import java.net.URI

interface Topic<IdType, T> : Publisher<T>, Subscriber<IdType, T> {

    val name: String
    val uri: URI?

}