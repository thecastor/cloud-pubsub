package de.thecastor.cloud.pubsub.google

import de.thecastor.cloud.pubsub.base.Configuration

data class PubSubConfiguration(val projectId: String,
                               val topicId: String
) : Configuration