package de.thecastor.cloud.pubsub.google

import com.google.cloud.pubsub.v1.TopicAdminClient
import com.google.protobuf.ByteString
import com.google.pubsub.v1.ProjectTopicName
import de.thecastor.cloud.pubsub.base.PubSubFactory
import de.thecastor.cloud.pubsub.base.Topic

class PubSubFactory : PubSubFactory<String, ByteString, PubSubConfiguration> {

    private val pubSubs = hashMapOf<ProjectTopicName, Topic<String, ByteString>>()

    override fun getTopic(config: PubSubConfiguration): Topic<String, ByteString> {
        val projectTopicName = ProjectTopicName.of(config.projectId, config.topicId)

        return pubSubs.getOrPut(projectTopicName) {
            return createTopic(config)
        }
    }

    override fun createTopic(config: PubSubConfiguration): Topic<String, ByteString> {
        val name = ProjectTopicName.of(config.projectId, config.topicId)
        val nativeTopic = TopicAdminClient.create().use { topicAdminClient -> topicAdminClient.createTopic(name) }

        return PubSubTopic(config, nativeTopic)
    }

}