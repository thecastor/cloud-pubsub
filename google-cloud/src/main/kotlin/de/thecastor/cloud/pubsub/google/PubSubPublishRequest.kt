package de.thecastor.cloud.pubsub.google

import com.google.protobuf.ByteString
import de.thecastor.cloud.pubsub.base.PublishRequest

class PubSubPublishRequest(override val data: ByteString) : PublishRequest<ByteString>