package de.thecastor.cloud.pubsub.google

import com.google.protobuf.ByteString
import de.thecastor.cloud.pubsub.base.AbstractPublishRequestBuilder

class PubSubPublishRequestBuilder : AbstractPublishRequestBuilder<ByteString>() {

    override fun setData(data: ByteArray) = setData(ByteString.copyFrom(data))

    fun setData(data: ByteString): PubSubPublishRequestBuilder {
        this.data = data

        return this
    }

    override fun build(): PubSubPublishRequest {
        val data = data ?: throw IllegalStateException("data is not set")

        return PubSubPublishRequest(data)
    }

}