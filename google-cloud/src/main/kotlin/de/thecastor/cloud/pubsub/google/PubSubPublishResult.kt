package de.thecastor.cloud.pubsub.google

import com.google.api.core.ApiFuture
import de.thecastor.cloud.pubsub.base.PublishCallback
import de.thecastor.cloud.pubsub.base.PublishResult

internal class PubSubPublishResult(private val future: ApiFuture<String>,
                                   private val callback: PublishCallback
) : PublishResult, PublishCallback by callback {

    var failure: Boolean = false
        internal set

    override fun onFailure(cause: Throwable) {
        failure = true

        callback.onFailure(cause)
    }

    override fun cancel() {
        future.cancel(true)
    }

    override fun isCancelled() = future.isCancelled

    override fun isSuccess() = isComplete() && !isFailure() && !isCancelled()

    override fun isFailure() = failure

    override fun isComplete() = future.isDone

}