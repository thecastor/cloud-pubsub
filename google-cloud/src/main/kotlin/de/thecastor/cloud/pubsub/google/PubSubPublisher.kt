package de.thecastor.cloud.pubsub.google

import com.google.api.core.ApiFutureCallback
import com.google.api.core.ApiFutures
import com.google.cloud.pubsub.v1.Publisher
import com.google.common.util.concurrent.MoreExecutors
import com.google.protobuf.ByteString
import com.google.pubsub.v1.ProjectTopicName
import com.google.pubsub.v1.PubsubMessage
import de.thecastor.cloud.pubsub.base.PublishCallback
import de.thecastor.cloud.pubsub.base.PublishRequest
import de.thecastor.cloud.pubsub.base.PublishResult
import java.util.concurrent.TimeUnit

internal class PubSubPublisher internal constructor(private val config: PubSubConfiguration) : de.thecastor.cloud.pubsub.base.Publisher<ByteString> {

    override fun requestBuilder() = PubSubPublishRequestBuilder()

    override fun sendAsync(publishRequest: PublishRequest<ByteString>, callback: PublishCallback): PublishResult {
        val publisher = Publisher.newBuilder(ProjectTopicName.of(config.projectId, config.topicId)).build()

        return try {
            val message = PubsubMessage.newBuilder().setData(publishRequest.data).build()
            val future = publisher.publish(message)
            val result = PubSubPublishResult(future, callback)

            ApiFutures.addCallback(future, object : ApiFutureCallback<String> {

                override fun onFailure(throwable: Throwable) = result.onFailure(throwable)

                // Once published, returns server-assigned message ids (unique within the topic)
                override fun onSuccess(messageId: String) = result.onSuccess(messageId)

            }, MoreExecutors.directExecutor())

            result
        } finally {
            publisher.shutdown()
            publisher.awaitTermination(1, TimeUnit.MINUTES)
        }
    }

}