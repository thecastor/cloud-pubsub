package de.thecastor.cloud.pubsub.google

import com.google.protobuf.ByteString
import com.google.pubsub.v1.Topic
import de.thecastor.cloud.pubsub.base.Publisher
import de.thecastor.cloud.pubsub.base.Subscriber
import de.thecastor.cloud.pubsub.google.sub.PubSubSubscriber
import java.net.URI

class PubSubTopic(config: PubSubConfiguration,
                  nativeTopic: Topic
) : de.thecastor.cloud.pubsub.base.Topic<String, ByteString>,
    Publisher<ByteString> by PubSubPublisher(config),
    Subscriber<String, ByteString> by PubSubSubscriber(config) {

    override val name = nativeTopic.name
    override val uri: URI? = null

}