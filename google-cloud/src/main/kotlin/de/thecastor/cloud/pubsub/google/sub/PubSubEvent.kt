package de.thecastor.cloud.pubsub.google.sub

import com.google.protobuf.ByteString
import de.thecastor.cloud.pubsub.base.Event

class PubSubEvent(override val id: String, override val data: ByteString) : Event<String, ByteString>