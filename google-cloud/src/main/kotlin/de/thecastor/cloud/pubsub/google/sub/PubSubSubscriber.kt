package de.thecastor.cloud.pubsub.google.sub

import com.google.protobuf.ByteString
import com.google.pubsub.v1.ProjectSubscriptionName
import de.thecastor.cloud.pubsub.base.EventProcessor
import de.thecastor.cloud.pubsub.base.Subscriber
import de.thecastor.cloud.pubsub.base.Subscription
import de.thecastor.cloud.pubsub.google.PubSubConfiguration

class PubSubSubscriber(private val config: PubSubConfiguration) : Subscriber<String, ByteString> {

    override fun subscribe(processor: EventProcessor<String, ByteString>): Subscription<ByteString> {
        return PubSubSubscription.of(ProjectSubscriptionName.of(config.projectId, config.topicId), processor)
    }

}