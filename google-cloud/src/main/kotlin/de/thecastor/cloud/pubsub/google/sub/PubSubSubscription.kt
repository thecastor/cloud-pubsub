package de.thecastor.cloud.pubsub.google.sub

import com.google.cloud.pubsub.v1.AckReplyConsumer
import com.google.cloud.pubsub.v1.MessageReceiver
import com.google.cloud.pubsub.v1.Subscriber
import com.google.protobuf.ByteString
import com.google.pubsub.v1.ProjectSubscriptionName
import com.google.pubsub.v1.PubsubMessage
import de.thecastor.cloud.pubsub.base.EventProcessor
import de.thecastor.cloud.pubsub.base.Subscription
import de.thecastor.cloud.pubsub.base.SubscriptionHostException

class PubSubSubscription private constructor(private val subscriber: Subscriber, private val processor: EventProcessor<String, ByteString>) : Subscription<ByteString> {

    private var _cancelled = false
    override val cancelled: Boolean
        get() = _cancelled

    override fun cancel() {
        if (_cancelled) return

        try {
            subscriber.stopAsync()
        } catch (t: Throwable) {
            processor.onError(SubscriptionHostException("Failure while stopping Subscriber", t))
        }
    }

    companion object {
        fun of(subscriptionName: ProjectSubscriptionName, processor: EventProcessor<String, ByteString>): PubSubSubscription {
            val subscriber: Subscriber = Subscriber.newBuilder(subscriptionName, object : MessageReceiver {
                override fun receiveMessage(message: PubsubMessage, consumer: AckReplyConsumer) {
                    processor.onEvent(PubSubEvent(message.messageId, message.data))
                }
            }).build().also {
                try {
                    // create a subscriber bound to the asynchronous message receiver
                    it.startAsync().awaitRunning()
                } finally {
                    it.stopAsync()
                }
            }

            return PubSubSubscription(subscriber, processor)
        }
    }

}